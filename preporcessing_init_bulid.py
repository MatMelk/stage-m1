# -*- coding: utf-8 -*-
"""
Created on Fri May  1 12:15:01 2020

@author: matmi
"""


import numpy as np

import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras.layers import Dense, Conv1D, Input, Lambda, Dropout
import tensorflow.keras.backend as K
from tensorflow.keras.callbacks import EarlyStopping

    
def preproc_data(data,img_size=32,sample=5):
    """
    
    Parameters
    ----------
    data : list
        liste de deux numpy array contenant les donnée puis les labels.
    img_size : int, optional
        taille des echantillons. The default is 32.
    sample : int, optional
        nombre d'echantillons par labels différents. The default is 5.

    Returns
    -------
    outA : numpy.array
        array contenant les donnée sous forme [sample,imgsize,:].
    labelsA : numpy.array
        arraycontenant les label associé a chaque sample.

    """
    labels=data[1]
    data=data[0]
    labelsA=np.zeros((0,labels.shape[1]))
    out=np.zeros((sample,img_size,data.shape[1]))
    outA=np.zeros_like(out)
    
    for i in np.unique(labels,axis=0): # Pour chaque label unique  
        
        subset=np.zeros((0,data.shape[1]))  # on genere un sous-goupe contenant 
                                            # toutes les donnée correspondante.
        for k in labels: # Pour le cas ou on aurait plusieurs label
            subset=np.row_stack((subset,data[np.where(np.array_equal(k,i))]))
        
        for j in range(sample): # pour chaque sample on genere un jeu d'indices aléatoire
            indice= np.random.choice(len(subset),size=img_size,replace=True)
            out[j,:,:]=subset[indice,:] # que l'on place ensuite dans une array
            labelsA=np.row_stack((labelsA,i))
        outA=np.concatenate((outA,out),axis=0)
    outA=outA[sample:] # Pour suprimer le premier groupe coposé de 0
    
    return outA,labelsA
   

def init_random(data, nlabels=10, patience=10,epochs=100,batch_size=32,lnodes=[128,64],ldrop=[0.5,0.25]):
    """

    Parameters
    ----------
    data : numpy.array
        Données.
    nlabels : int, optional
        nombre de label aléatoir créé. The default is 10.
    patience : int, optional
        parametre du modèle. The default is 10.
    epochs : int, optional
        nombre d'iteration lors de l'entrainement. The default is 100.
    batch_size : int, optional
        nombre de donnée simultanément utilisé lors de l'entrainement. The default is 32.
    lnodes : list, optional
        liste du nombre de noeuds de chaque couches Denses que l'on souhaite créé. The default is [128,64].
    ldrop : list, optional
        list de la valeur de dropout qui suit chaque couches Denses. The default is [0.5,0.25].

    Returns
    -------
    encoder : tf.Model
        Modèle de la couche input a la derniere couche dropout (sans la couche output).

    """
    labels = tf.keras.utils.to_categorical(np.random.randint(low=nlabels,size=data.shape[0], dtype='l')) # généré des labels
    es = EarlyStopping(monitor='loss', mode='min', restore_best_weights=True, patience=patience)
    
    input=Input(shape=(data.shape[1],))
    x=input
    
    for i in range(len(lnodes)): # Création d'une couche Dense et dropout par itération
        x=Dense(lnodes[i],activation='relu')(x)
        x=Dropout(ldrop[i])(x)
    encoder=Model(input,x) # Modele envoyé en sortie
    output=Dense(nlabels,activation='softmax')(x)
    model=Model(input,output) # Modele qui compilé et entrainé
    model.compile(optimizer=tf.keras.optimizers.Adam(), loss=['categorical_crossentropy'],metrics=['accuracy'])
    model.fit(data,labels,batch_size=batch_size,epochs=epochs,callbacks=[es],shuffle=True,verbose=1)
    
    return encoder

def select_top(x, k):
    return K.mean(tf.sort(x, axis=1)[:, -k:, :], axis=1)

def masked_loss_function(y_true, y_pred):
    """
    Fonction loss similaire a la crossentropy mais en excluant les valeur masqué ici -1
    """
    mask = K.cast(K.not_equal(y_true,-1), K.floatx())
    return K.binary_crossentropy(y_true * mask, y_pred * mask)

def masked_accuracy(y_true, y_pred):
    """
    Fonction accuracy similaire a la l'accuracy mais en excluant les valeur masqué ici -1
    """
    mask=K.cast(K.not_equal(y_true,-1),K.floatx())
    nb_mask=K.sum(K.mean(K.equal(y_true,-1)))
    return (K.sum(K.mean(K.equal(mask*y_true,K.round(mask*y_pred))))-nb_mask)/(1-nb_mask)

def masked_accuracy2(y_true, y_pred): 
    """
    Fonction accuracy similaire a la l'accuracy mais en excluant les valeur masqué ici -1
    """
    mask=K.cast(K.not_equal(y_true,-1),K.floatx())
    nb_mask=K.sum(K.cast(K.equal(y_true,-1),K.floatx()))
    return (K.sum(K.cast(K.equal(mask*y_true, K.round(mask*y_pred)),K.floatx()))-nb_mask)/K.sum(mask)



def build_model_from_encoder(data,labels,encoder,patience=10,k=10,epochs=1000,nfilter=5,replace=True):
    """

    Parameters
    ----------
    data : numpy.array
        DESCRIPTION.
    labels : numpy.array
        DESCRIPTION.
    encoder : tf.Model
        DESCRIPTION.
    patience : int, optional
        parametre du modèle. The default is 10.
    epochs : int, optional
        nombre d'iteration lors de l'entrainement. The default is 1000.
    nfilter : int, optional
        nombre filtre utilisé. The default is 5.
    replace : bool, optional
        active ou desactive le remplacement des poids. The default is True.

    Returns
    -------
    model : tf.Model
        Modèle Convolutionnel généré a l'aide des poids du modèle Dense.

    """
    es=EarlyStopping(monitor='loss', mode='min', restore_best_weights=True, patience=patience)

    input=Input(shape=(data.shape[1],data.shape[2]))
    x=input
    for i in encoder.layers: # Création du modèle apartir des information d'encoder
        if type(i)==type(Dense(1)):
            x=Conv1D(filters=i.units,kernel_size=1,activation=i.activation)(x)
        if type(i)==type(Dropout(0.5)):
            x=Dropout(i.get_config()['rate'])(x)
        weights=i.get_weights() #transfert des poids
        if weights!=[] and replace==True:
            weights[0]=np.expand_dims(weights[0], axis=0)
            coder=Model(input,x)
            coder.layers[len(coder.layers)-1].set_weights(weights)
        
    x=Conv1D(nfilter,kernel_size=1,activation="relu")(x)
    pooled = Lambda(select_top, output_shape=(1,), arguments={'k':k})(x)
    output=Dense(labels.shape[1],activation="sigmoid")(pooled) #ajout de la couche d'output
    model=Model(input,output)
    
    # Compilation et entrainement du modèle
    model.compile(optimizer='adam', loss=masked_loss_function, metrics=['accuracy',masked_accuracy,masked_accuracy2])
    model.fit(data,labels,batch_size=10,epochs=epochs,callbacks=[es],verbose=1)
    
    return model

def model_single_cell(encoder):
    """

    Parameters
    ----------
    encoder : tf.Model
        Modèle Convolutionnel entrainé.

    Returns
    -------
    model : tf.Model
        Modèle Dense non entrainé permetant de faire des prédiction.

    """

    input=Input(shape=(encoder.layers[0].get_input_shape_at(0)[2],))
    x=input
    for i in encoder.layers: #Création du modèle a partir des information d'encoder
        if type(i)==type(Conv1D(1,1)):
            x=Dense(i.filters,activation=i.activation)(x)
            weights=i.get_weights()
            weights[0]=np.squeeze(weights[0],axis=0)
            coder=Model(input,x)
            coder.layers[len(coder.layers)-1].set_weights(weights) 
    model=Model(input,x)
    
    return model


    

